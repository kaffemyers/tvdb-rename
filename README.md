# tvdb-rename

Bash script to rename video media files based on what is available on TheTVDB.com

No matter the state of the script, use it at your own risk. I take no responsibility.

That being said, the script should currently be seen as being in beta. Will amend this readme later, but for now, run the script with -h flag to get help!

Must haves:
TV information is provided by TheTVDB.com, but we are not endorsed or certified by TheTVDB.com or its affiliates.
